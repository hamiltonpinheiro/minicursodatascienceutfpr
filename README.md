# MiniCursoDataScienceUTFPR 

MiniCursoDataScienceUTFPR 


Mini Curso sobre Data Science realizado na UTFPR Dois Vizinhos  em 03 e 04 de Setembro de 2019 na IV SAES semana acadêmica de engenharia de software.

O conteúdo pre-supõe que você possui o ambiente configurado e as libs instaladas, caso não tenha isso ainda  instale o https://jupyter.org/install e os as libs da ultima
página do .pdf ;) ou execute pip install -R requeriments.txt "For linux"

Para entender melhor o jupyter abra e olhe os aquivos na seguinte ordem:

Pasta Introdução, 
Pasta Image-to-text, 
Pasta Image-recognition

Pasta UTFPR contem os exemplos feito em sala.

Os datasets .CSV são arquivos dispiníveis no https://www.kaggle.com/datasets

Enjoy ;)